# Sobre nosotros

## Equipo

El equipo de Feria App se compone de los siguientes integrantes de Arquitectura de Software (ICC368-1) 1er semestre 2022 de la Universidad de la Frontera

* Kianush Atighi-Moghaddam
* Arturo Avendaño
* Lorenzo Devia
* Lucas Palminio
* Pablo Nahuelpan
* Rodolfo Hernández
* Dante Álvarez
* Diego Vera
* Bruno Hernández
* Gabriel Ruiz
* Alonso Rojas



Donde se presenta el material utilizado para armar la construcción y la planificación de como construir una app móvil.
El cuál se constituye de diagramas y prototipos que sirven como documentación para la toma de decisiones y crear la aplicación.
## Visión

Diseñar una plataforma que ponga a la vista los productores de la región que no estan muy presente a nivel público.
