# Casos de uso

<h2>Casos de Usos Identificados</h2>

  Modelo UML de Casos de Uso donde se muestra el comportamiento deseado del sistema.

  ![UseCase](img/CasosDeUso.png)
  