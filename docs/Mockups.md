# Mockups

<h1>Prototipo de FeriaApp Responsiva</h1>

Prototipo de la Aplicación de FeriaApp realizada en Figma, la cual posee caracteristicas responsivas acorde al area tocada.

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FQb6SSWEMJ8QXEHnIXxDnWR%2FFeria-app%3Fnode-id%3D168%253A264%26scaling%3Dscale-down%26page-id%3D0%253A1%26starting-point-node-id%3D168%253A264" allowfullscreen></iframe>

<br>

<h1>Mockups de Feria App</h1>

Mockups de la aplicación de FeriaApp, donde se muestra la meta de desarrollo a alcanzar.

  <h2>Mockup Pagina Principal</h2>

Primera vista que se veria al iniciar la aplicación de FeriaApp

  ![PaginaPrincipal](img/Figma/Paginaprincipal.png)
  
  <h2>Mockup Pagina Producción Mensual</h2>

Vista la cual muestra la producción mensual de un productor.  

  ![ProduccionMensual](img/Figma/P%C3%A1ginaProduccion.png)
  
  <h2>Mockup Carro de Compras</h2>

Vista en donde se almacenan todos los productos a comprar.

  ![CarroDeCompras](img/Figma/CarroCompras.png)

  <h2>Mockup Modificación Curso en Linea</h2>

Vista de modificación de los detalles de un Curso en Linea que se encuentre activo. 

  ![CursosDisponibles](img/Figma/P%C3%A1gina%20modificar%20curso%20disponible.png)

  <h2>Mockup Chat Activos</h2>

Vista en donde se muestra todos los Chats activos del usuario

  ![Chat](img/Figma/Chat.png)

  <h2>Mockup Conversación mediante Chat</h2>

Vista en donde ocurre la comunicación entre 2 usuarios siendo estos Productor y/o Comprador

  ![MensajeriaConRodolfo](img/Figma/ChatAbierto.png)

  <h2>Mockup Pagina del Perfil de Usuario</h2>

Vista del perfil del usuario de la cuenta, en donde se podran escoger diferentes opciones.

  ![PerfilUsuario](img/Figma/Perfil.png)

  <h2>Mockup Pagina de los productos en venta</h2>

Vista unica de un productos en la cual se muestran todos los productos a la venta de este usuario.

  ![PerfilUsuario](img/Figma/ProductosALaVenta.png)

  

