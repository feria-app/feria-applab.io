# Bienvenido a la documentación de Feria App

En esta página encontrarás la documentación respecto al diseño de la plataforma Feria App, cuyo objetivo es proveer medios para efectuar la compra y venta de recursos agrícolas, la publicidad de productores locales para dar a conocer sus negocios y la comunicación entre productores y compradores para agilizar la coordinación entre ambas partes.
La documentación se divide en:

- Información respecto a nosotros como agrupación y nuestra visión con respecto a la plataforma de Feria App
- Lista de stakeholders identificados y como sus objetivos se relacionan con los requerimientos funcioanles y aspectos de calidad de la plataforma
- Lista de requerimientos funcionales que provee la plataforma para satisfacer los objetivos que posee cada stakeholder 
- Lista de requerimientos no funcionales asociados a aspectos de calidad y restricciones que debe cumplir para satisfacer las necesidades de los stakeholders de manera adecuada
- Historias de usuario clasificadas por su complejidad, tiempo de desarrollo, nivel de reuso entre otros aspectos
- Vista lógica del modelo C4+1 
- Mockups de la plataforma que muestran el aspecto de la interfaz gráfica de Feria App

