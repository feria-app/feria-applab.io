# Visual Ad-hoc Methodology

La metodología a utilizar será llamada “Visual Ad-hoc Methodology”, la cual está inspirada en el trabajo que realizan los dibujantes de grandes compañías como Disney y Pixar al realizar sus películas o cortometrajes, pero aplicada al desarrollo de software.

Se relaciona con el concepto de "StoryBoard", donde se expone cada escena o frame de la película completa, acompañada de detalles y comentarios que ayudan a comprender contexto general.

Esto, en conjunto con otras técnicas de educción y diseño conforman la metodología a utilizar.

![Metodologia](img/adhoc.png)

Se describirán los pasos necesarios para desarrollar la metodología:


## Paso 1: Stakeholders

En este paso será definidos los stakeholders asociados al proyecto, de los cuales se obtendrá la información pertinente para continuar el proceso.

## Paso 2: Vision, objetivos y áreas

A través de un método de educción se detectará y segmentará la problemática y contexto en torno a estos tres elementos: Visión, Objetivos y Áreas. Este paso permite entender y definir el dominio del problema, el alcance y las expectativas.

## Paso 3: Historias de usuario por áreas

En base a la información recopilada hasta este momento, se detallarán historias de usuario para cada una de las Áreas o tópicos en cuestión.

## Paso 4: Selección de historias de usuario

De las historias de usuario generadas, en base a su prioridad, se seleccionarán las más relevantes o críticas. Esta selección permitirá definir una primera versión del sistema a desarrollar.

## Paso 5: Mockups

Utilizando una herramienta de modelado de interfaces, se crearán las pantallas con las que interactua el usuario, las cuales más adelante serán implementadas en el sistema final.

## Paso 6: Diseño del sistema

En la etapa de diseño del sistema, se busca especificar las entidades relevantes para posteriormente desarrollar una implementación dirigida por modelos (MDD). El MDD necesita como insumo básicamente un diagrama entidad relación, el cual deberá estar en concordancia con las historias de usuario seleccionadas y el diseño de interfaz.

## Paso 7: Implementación y testing

La implementación se refiere a la fase del proyecto en que se llevan a cabo las tareas necesarias para darle vida a la plataforma, las tecnologías a utilizar deberán ser seleccionadas de acuerdo a los requerimientos del proyecto y capacidades del equipo. EL testing por su parte hace referencia a las pruebas del sistema de manera de asegurar la calidad de este.

## Paso final: Deployment

Corresponde a la fase en la cual el sistema se despliega para su uso final.
